// TrabalhoOtimização.cpp : Este arquivo contém a função 'main'. A execução do programa começa e termina ali.
//
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <stdlib.h>
#include <memory>
#include <time.h>
#include <math.h>
#include "Solucao.h"

//#define EXIBIR_MELHOR


int main()
{
	Solucao solucao, solucao1;
	char file[25] = "A-n32-k5";
#ifdef EXIBIR_MELHOR
	lerSolucao(file, solucao1);
	exibirSolucao(solucao1);
#endif // EXIBIR_MELHOR
	lerArquivo(file);
	if (tipoArquivo == 0) {
		calculaCusto();
	}
	int repeticoes = 1, h = 0;
	double tempo;
	h = clock();
	for (int r = 0; r < repeticoes; r++) {
		heuristicaConGul(solucao);
		calculoFO(solucao);
	}
	h = clock() - h;
	tempo = h / (double)CLOCKS_PER_SEC;
	std::cout << "Tempo: " << tempo << std::endl;
	exibirSolucao(solucao);
	system("pause");

}

void heuristicaConGul(Solucao& solucao) {
	//Limpeza das estruturas da solução
	memset(solucao.rota, -1, sizeof(solucao.rota));
	memset(solucao.carroCliente, 0, sizeof(solucao.carroCliente));
	memset(solucao.carroPeso, 0, sizeof(solucao.carroPeso));
	int clientesNaoAtendidos[NUM_CLIENTES],     //Clientes ainda não atendidos
		clienteAtual[NUM_CARROS],				//Cliente atualmente atendido por determinado carro
		melhorCB,								//Cliente com a distância mais curta
		melhorCBpos,							//Posição correspondente ao cliente no vetor clientesNaoAtendidos
		numClientesNaoAtendidos,				//Número total de clientes ainda não atendidos
		aux;
	bool cabe;
	numClientesNaoAtendidos = numClientes - 1;
	//Zerando o vetor de clientes não atendidos
	memset(clienteAtual, 0, sizeof(clienteAtual));
	//atribuindo os valores dos clientes no vetor de clientes não atendidos
	for (int i = 0; i < numClientes; i++) {
		clientesNaoAtendidos[i] = i + 1;
	}
	//Colocando os clientes mais pesados nos carros (procedimento utilizado para não sobrar
	//cliente muito pesado no final e não caber em nenhum carro entrando assim em loop)
	for (int i = 0; i < numCarros; i++) {
		melhorCB = clientesNaoAtendidos[0];
		melhorCBpos = 0;
		for (int j = 1; j < numClientesNaoAtendidos; j++) {
			if (clientes[clientesNaoAtendidos[j] - 1].peso > clientes[melhorCB - 1].peso) {
				melhorCB = clientesNaoAtendidos[j];
				melhorCBpos = j;
			}
		}
		solucao.rota[i][solucao.carroCliente[i]] = melhorCB;
		solucao.carroCliente[i]++;
		aux = clientesNaoAtendidos[numClientesNaoAtendidos - 1];
		clientesNaoAtendidos[numClientesNaoAtendidos - 1] = clientesNaoAtendidos[melhorCBpos];
		clientesNaoAtendidos[melhorCBpos] = aux;
		numClientesNaoAtendidos--;
		clienteAtual[i] = melhorCB;
		solucao.carroPeso[i] += clientes[melhorCB - 1].peso;
	}
	//Atribuindo os clientes mais próximos aos carros
	while (numClientesNaoAtendidos > 0) {
		for (int i = 0; i < numCarros; i++) {
			cabe = false;
			//Testando se os clientes não acabaram
			if (numClientesNaoAtendidos == 0) {
				break;
			}
			//Atribuindo um valor inicial
			melhorCB = clientesNaoAtendidos[0];
			melhorCBpos = 0;
			//Pegando o clientes mais próximo do ponto em que o carro se encontra
			for (int j = 0; j < numClientesNaoAtendidos; j++) {
				if (custo[clienteAtual[i]][clientesNaoAtendidos[j] - 1] <= custo[clienteAtual[i]][melhorCB - 1]) {
					if ((solucao.carroPeso[i] + clientes[clientesNaoAtendidos[j] - 1].peso) <= capacidade) {
						melhorCB = clientesNaoAtendidos[j];
						melhorCBpos = j;
						cabe = true;
					}
				}
			}
			//teste para saber se o carro comporta o cliente
			if (cabe) {
				//Adiciona o cliente
				solucao.rota[i][solucao.carroCliente[i]] = melhorCB;
				//Aumenta o número de clientes no carro
				solucao.carroCliente[i]++;
				//Efetuando a troca com o último elemento do vetor de clientes não atendidos
				aux = clientesNaoAtendidos[numClientesNaoAtendidos - 1];
				clientesNaoAtendidos[numClientesNaoAtendidos - 1] = clientesNaoAtendidos[melhorCBpos];
				clientesNaoAtendidos[melhorCBpos] = aux;
				//Diminui o número de clientes não atendidos
				numClientesNaoAtendidos--;
				//Altera o cliente atual do carro i
				clienteAtual[i] = melhorCB;
				//Aumenta o peso do carro
				solucao.carroPeso[i] += clientes[melhorCB - 1].peso;
			}
		}
	}
}

void lerArquivo(char* file) {
	FILE* fp;
	tipoArquivo = -1;
	char file_name[25];
	int i = 0;
	while (i < strlen(file)) {
		file_name[i] = file[i];
		i++;
	}
	file_name[i] = '.';
	file_name[i + 1] = 'v';
	file_name[i + 2] = 'r';
	file_name[i + 3] = 'p';
	file_name[i + 4] = '\0';
	fp = fopen(file_name, "r"); //modo leitura
	if (fp == NULL)
	{
		perror("Erro ao abrir o arquivo.\n");
		exit(EXIT_FAILURE);
	}

	while (!feof(fp)) {
		char mystring[100];
		fgets(mystring, 100, fp);
		if (strncmp(mystring, "NAME : ", strlen("NAME : ")) == 0) {
			//CAPTA O NUMERO DE VEICULOS
			char aux[100];
			strcpy(aux, mystring);
			int i = strlen("NAME : "), j = 0;
			char aux2[100];
			while (i < strlen(aux)) {
				aux2[j] = aux[i];
				i++;
				j++;
			}
			aux2[j] = '\0';
			char* p = strchr(aux2, 'k');
			p[0] = '0';
			numCarros = atoi(p);
		}
		else if (strncmp(mystring, "DIMENSION : ", strlen("DIMENSION : ")) == 0) {
			//CAPTA O NUMERO DE CLIENTES
			char aux[100];
			strcpy(aux, mystring);
			int i = strlen("DIMENSION : "), j = 0;
			char aux2[100];
			while (i < strlen(aux)) {
				aux2[j] = aux[i];
				i++;
				j++;
			}
			aux2[j] = '\0';
			numClientes = atoi(aux2);
		}
		else if (strncmp(mystring, "CAPACITY : ", strlen("CAPACITY : ")) == 0) {
			//CAPTA A CAPACIDADE DOS VEICULOS
			char aux[100];
			strcpy(aux, mystring);
			int i = strlen("CAPACITY : "), j = 0;
			char aux2[100];
			while (i < strlen(aux)) {
				aux2[j] = aux[i];
				i++;
				j++;
			}
			aux2[j] = '\0';
			capacidade = atoi(aux2);
		}
		else if (strncmp(mystring, "NODE_COORD_SECTION", strlen("NODE_COORD_SECTION")) == 0) {
			//ler até "DEMAND_SECTION"
			fgets(mystring, 100, fp);
			while (strncmp(mystring, "DEMAND_SECTION", strlen("DEMAND_SECTION")) != 0) {
				//cada linha
				char* pch;
				pch = strtok(mystring, " ");
				int i = atoi(pch);
				clientes[i - 1].num = i;
				pch = strtok(NULL, " ");
				int j = atoi(pch);
				clientes[i - 1].x = j;
				pch = strtok(NULL, " ");
				j = atoi(pch);
				clientes[i - 1].y = j;
				fgets(mystring, 100, fp);
			}
			//ler até "DEPOT_SECTION"
			fgets(mystring, 100, fp);
			while (strncmp(mystring, "DEPOT_SECTION", strlen("DEPOT_SECTION")) != 0) {
				//cada linha
				char* pch;
				pch = strtok(mystring, " ");
				int i = atoi(pch);
				pch = strtok(NULL, " ");
				int j = atoi(pch);
				clientes[i - 1].peso = j;
				fgets(mystring, 100, fp);
			}
			tipoArquivo = 0;
		}
		else if (strncmp(mystring, "EDGE_WEIGHT_SECTION", strlen("EDGE_WEIGHT_SECTION")) == 0) {
			//ler até "DEMAND_SECTION"
			while (strncmp(mystring, "DEMAND_SECTION", strlen("DEMAND_SECTION")) != 0) {
				//cada linha
				char* pch = NULL;
				for (int x = 0; x < numClientes - 1; x++) {
					for (int y = x + 1; y < numClientes; y++) {
						if (pch == NULL) {
							fgets(mystring, 100, fp);
							pch = strtok(mystring, " ");
						}
						else {
							pch = strtok(NULL, " ");
						}
						if (pch == NULL) {
							y--;
						}
						else {
							custo[x][y] = atoi(pch);
							custo[y][x] = atoi(pch);
						}
					}
				}
				fgets(mystring, 100, fp);
			}
			tipoArquivo = 1;
			//ler até "DEPOT_SECTION"
			fgets(mystring, 100, fp);
			while (strncmp(mystring, "DEPOT_SECTION", strlen("DEPOT_SECTION")) != 0) {
				//cada linha
				char* pch;
				pch = strtok(mystring, " ");
				int i = atoi(pch);
				pch = strtok(NULL, " ");
				int j = atoi(pch);
				clientes[i - 1].peso = j;
				fgets(mystring, 100, fp);
			}
		}
	}
	fclose(fp);
}

void lerSolucao(char* file, Solucao& solucao) {
	FILE* fp;
	char file_name[25];
	int i = 0;
	while (i < strlen(file)) {
		file_name[i] = file[i];
		i++;
	}
	file_name[i] = '.';
	file_name[i + 1] = 's';
	file_name[i + 2] = 'o';
	file_name[i + 3] = 'l';
	file_name[i + 4] = '\0';
	int num_cliente = 0;
	numCarros = 0;
	numClientes = 1;
	fp = fopen(file_name, "r"); //modo leitura
	if (fp == NULL)
	{
		perror("Erro ao abrir o arquivo.\n");
		exit(EXIT_FAILURE);
	}

	while (!feof(fp)) {
		char mystring[100];
		fgets(mystring, 100, fp);
		if (strncmp(mystring, "Route #", strlen("Route #")) == 0) {
			//CAPTA A ROTA
			char aux[100], aux2[100], aux3[100];
			strcpy(aux, mystring);
			int i = strlen("Route #"), j = 0, veiculo = 1, k = 0;
			while (i < strcspn(aux, ":")) {
				aux2[j] = aux[i];
				i++;
				j++;
			}
			veiculo = atoi(aux2);
			numCarros++;
			i = strcspn(aux, ":") + 2, j = 0;
			while (i < strlen(aux)) {
				aux3[j] = aux[i];
				i++;
				j++;
			}
			aux3[j] = '\0';
			char* pch = NULL;
			pch = strtok(aux3, " ");
			while (pch != NULL && strcmp(pch, "\n")) {
				solucao.rota[veiculo - 1][k] = atoi(pch);
				pch = strtok(NULL, " ");
				k++;
				num_cliente++;
			}
			solucao.carroCliente[veiculo - 1] = num_cliente;
			numClientes += num_cliente;
			num_cliente = 0;
		}
		else if (strncmp(mystring, "Cost ", strlen("Cost ")) == 0) {
			//CAPTA O CUSTO
			char aux[100], aux2[100];
			strcpy(aux, mystring);
			int i = strlen("Cost "), j = 0;
			while (i < strlen(aux)) {
				aux2[j] = aux[i];
				i++;
				j++;
			}
			solucao.ResultFO = atoi(aux2);
		}
	}
	fclose(fp);
}

void escreverEmArquivo(char* file_name, Solucao solucao) {
	FILE* fp;
	int i = 0;
	char file_name_sol[25], mystring[100], aux[100];
	while (i < strcspn(file_name, ".")) {
		file_name_sol[i] = file_name[i];
		i++;
	}
	file_name_sol[i] = '\0';
	strcat(file_name_sol, ".txt");
	fp = fopen(file_name_sol, "w");
	if (fp == NULL)
	{
		perror("Erro ao abrir o arquivo.\n");
		exit(EXIT_FAILURE);
	}
	for (i = 0; i < numCarros; i++) {
		strcpy(mystring, "Route #");
		_itoa(i + 1, aux, 10);
		strcat(mystring, aux);
		strcat(mystring, ": ");
		for (int j = 0; j < solucao.carroCliente[i]; j++) {
			_itoa(solucao.rota[i][j] + 1, aux, 10);
			strcat(mystring, aux);
			strcat(mystring, " ");
		}
		strcat(mystring, "\n");
		fputs(mystring, fp);
	}
	strcpy(mystring, "Cost ");
	_itoa(solucao.ResultFO, aux, 10);
	strcat(mystring, aux);
	fputs(mystring, fp);

	fclose(fp);
}

void calculaCusto() {
	memset(&custo, -1, sizeof(custo));
	for (int i = 0; i < numClientes; i++) {
		for (int j = 0; j < numClientes; j++) {
			custo[i][j] = distancia(clientes[i], clientes[j]);
		}
	}
}

void calculoFO(Solucao& solucao) {
	solucao.ResultFO = 0;
	for (int i = 0; i < numCarros; i++) {
		solucao.ResultFO += custo[0][solucao.rota[i][0]];
		for (int j = 0; j < solucao.carroCliente[i] - 1; j++) {
			solucao.ResultFO += custo[solucao.rota[i][j]][solucao.rota[i][j + 1]];
		}
		solucao.ResultFO += (custo[solucao.rota[i][solucao.carroCliente[i] - 1]][0]);
	}
}

float distancia(Cliente pontoX, Cliente pontoY) {
	float quadradoDosCatetos = ((pontoX.x - pontoY.x) * (pontoX.x - pontoY.x)) + ((pontoX.y - pontoY.y) * (pontoX.y - pontoY.y));
	return sqrt(quadradoDosCatetos);
}

void exibirSolucao(Solucao& solucao) {
	std::cout << "Custo: " << solucao.ResultFO << std::endl;
	for (int i = 0; i < numCarros; i++) {
		std::cout << "Peso carro " << i << ": " << solucao.carroPeso[i] << std::endl;
	}
	std::cout << "Rota:" << std::endl;
	for (int i = 0; i < numCarros; i++) {
		std::cout << "Carro " << i << ": " << "1;";
		for (int j = 0; j < solucao.carroCliente[i]; j++) {
			std::cout << solucao.rota[i][j] + 1 << ";";
		}
		std::cout << "1" << std::endl;
	}
}

void clonar(Solucao& solucaoC, Solucao& solucaoV) {
	memset(solucaoV.rota, -1, sizeof(solucaoV.rota));
	memset(solucaoV.carroCliente, 0, sizeof(solucaoV.carroCliente));
	memset(solucaoV.carroPeso, 0, sizeof(solucaoV.carroPeso));
	for (int i = 0; i < numCarros; i++) {
		solucaoV.carroCliente[i] = solucaoC.carroCliente[i];
		solucaoV.carroPeso[i] = solucaoC.carroPeso[i];
	}
	for (int i = 0; i < numCarros; i++) {
		for (int j = 0; j < numClientes; j++) {
			solucaoV.rota[i][j] = solucaoC.rota[i][j];
		}
	}
	solucaoV.ResultFO = solucaoC.ResultFO;
}

// Executar programa: Ctrl + F5 ou Menu Depurar > Iniciar Sem Depuração
// Depurar programa: F5 ou menu Depurar > Iniciar Depuração

// Dicas para Começar: 
//   1. Use a janela do Gerenciador de Soluções para adicionar/gerenciar arquivos
//   2. Use a janela do Team Explorer para conectar-se ao controle do código-fonte
//   3. Use a janela de Saída para ver mensagens de saída do build e outras mensagens
//   4. Use a janela Lista de Erros para exibir erros
//   5. Ir Para o Projeto > Adicionar Novo Item para criar novos arquivos de código, ou Projeto > Adicionar Item Existente para adicionar arquivos de código existentes ao projeto
//   6. No futuro, para abrir este projeto novamente, vá para Arquivo > Abrir > Projeto e selecione o arquivo. sln
